const request = require('request');

const geocodeAddress = (address, callback) => {

    let encodedAddress = encodeURIComponent(address);
    console.log(encodedAddress)

    request({
        uri: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=AIzaSyCwmhBh40IMgbJnpBJeEwCxE-FQVSxIbK8`,
        json: true
    }, (err, response, body) => {

        if (err) {
            
            callback('Unable to connect to google servers');
        } else if (body.status === 'ZERO_RESULTS') {
            
            callback('No results found for provided address');
        } else if (body.status === 'OK') {

            callback(undefined, {
                address: body.results[0].formatted_address,
                latitude: body.results[0].geometry.location.lat,
                longitude: body.results[0].geometry.location.lng
            });
        }

    });
}

module.exports = {
    geocodeAddress
};